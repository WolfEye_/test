<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function (){
    Route::resource('project', 'ProjectsController');
    Route::resource('user', 'UsersController')->only([
        'index', 'update', 'userImage'
    ]);

    Route::delete('project/fileDestroy/{id}', 'ProjectsController@fileDestroy');

    Route::get('csvCreate', 'ProjectsController@csvCreate')->name('project.csvCreate');
    Route::post('csvCreate', 'ProjectsController@csvCreateStore')->name('project.csvCreate.store');
    Route::get('projectToPDF/{id}', 'ProjectsController@projectToPDF')->name('project.projectToPDF');
    Route::get('allProjectsToCSV', 'ProjectsController@allProjectsToCSV')->name('project.allProjectsToCSV');

    Route::post('userImage', 'UsersController@userImage')->name('user.image');
});
