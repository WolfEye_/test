@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <table id="dtMaterialDesignExample" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Organization</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Role</th>
                    <th>Link</th>
                    <th>Type</th>
                    <th>Skills</th>
                    <th>Files</th>
                    <th>Created_at</th>
                    <th>Updated_at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>{{ $project->id }}</th>
                    <th>{{ $project->name }}</th>
                    <th>
                        <a href="#" data-toggle="modal"
                           data-target="#description">Description</a>

                        <div class="modal fade" id="description" tabindex="-1"
                             role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Description</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-right">
                                        {{ $project->description }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>
                    <th>{{ $project->organization }}</th>
                    <th>{{ $project->start }}</th>
                    <th>{{ $project->end }}</th>
                    <th>{{ $project->role }}</th>
                    <th>
                        <a href="{{ $project->link }}" target="_blank">{{ $project->link }}</a>
                    </th>
                    <th>{{ $project->type }}</th>
                    <th>
                        <a href="#" data-toggle="modal"
                           data-target="#skills">Skills</a>

                        <div class="modal fade" id="skills" tabindex="-1"
                             role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Skills</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-right">
                                        <ul>
                                            @foreach($project->skills as $skill)
                                                <li>{{ $skill->name }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>
                    <th>
                        <a href="#" data-toggle="modal"
                           data-target="#files">Files</a>

                        <div class="modal fade" id="files" tabindex="-1"
                             role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Files</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-right">
                                        <ul>
                                            @foreach($project->files as $file)
                                                <li>{{ $file->name }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>
                    <th>{{ $project->created_at }}</th>
                    <th>{{ $project->updated_at }}</th>
                    <td class="text-right">
                        <div class="btn-group text-left">
                            <a href="{{ route('project.projectToPDF', $project->id) }}"
                               class="btn btn-success btn-sm"
                               id="downloadPDF"
                               data-toggle="tooltip"
                               data-placement="top"
                               title="Download as PDF"><i class="fas fa-download"></i></a>
                            <a href="{{ route('project.edit', $project->id) }}"
                               class="btn btn-primary btn-sm" data-toggle="tooltip"
                               data-placement="top" title="Edit"><i class="fas fa-edit"></i></a>

                            @if(auth()->user()->isAdmin())
                                <a href="#" class="btn btn-sm btn-danger" data-toggle="modal"
                                   data-target="#questionModal-{{ $project->id }}" title="Remove"><i class="fas fa-trash"></i></a>

                                <div class="modal fade" id="questionModal-{{ $project->id }}" tabindex="-1"
                                     role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Вы уверены что хотите удалить?</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-right">
                                                <a class="btn btn-sm btn-danger deleteButton"
                                                   data-dismiss="modal"
                                                   data-id="{{$project->id}}"><i class="fas fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(() => {
            $.noConflict();
            $('#dtMaterialDesignExample').DataTable({
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
            });
            $('.deleteButton').on('click', (e) => {
                const projectId = $(e.currentTarget).data('id');
                $.ajax({
                    type: 'delete',
                    url: '{{url("project/")}}' + '/' + projectId,
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    data: {},
                    success: function (response) {
                        window.location.replace('{{url("project/")}}');
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });
        });
    </script>
@endsection
