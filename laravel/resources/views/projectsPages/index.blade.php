@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <table id="dtMaterialDesignExample" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="th-sm">#</th>
                    <th class="th-sm">Title</th>
                    <th class="th-sm">Description</th>
                    <th class="th-sm">Organization</th>
                    <th class="th-sm">Start</th>
                    <th class="th-sm">End</th>
                    <th class="th-sm">Role</th>
                    <th class="th-sm">Link</th>
                    <th class="th-sm">Type</th>
                    <th class="th-sm">Created_at</th>
                    <th class="th-sm">Updated_at</th>
                    <th class="th-sm"></th>
                </tr>
                </thead>
                <tbody>
                @if ($projects->total() === 0)
                    <tr>
                        <td colspan="3" class="text-center">Empty...</td>
                    </tr>
                @else
                    @foreach($projects->items() as $item)
                        <tr>
                            <th>{{ $item->id }}</th>
                            <th>
                                <a href="{{ route('project.show', $item->id) }}">{{ $item->name }}</a>
                            </th>
                            <th>
                                <div class="dropdown show">
                                    <a class="dropdown-toggle" href="#" role="button"
                                       id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">Description</a>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="width: 500px;">
                                        {{ $item->description }}
                                    </div>
                                </div>
                            </th>
                            <th>{{ $item->organization }}</th>
                            <th>{{ $item->start }}</th>
                            <th>{{ $item->end }}</th>
                            <th>{{ $item->role }}</th>
                            <th>
                                <a href="{{ $item->link }}" target="_blank">{{ $item->link }}</a>
                            </th>
                            <th>{{ $item->type }}</th>
                            <th>{{ $item->created_at }}</th>
                            <th>{{ $item->updated_at }}</th>
                            <td class="text-right">
                                <div class="btn-group text-left">
                                    <a href="{{ route('project.edit', $item->id) }}"
                                       class="btn btn-primary btn-sm" data-toggle="tooltip"
                                       data-placement="top" title="Редактировать"><i class="fas fa-edit"></i></a>

                                    @if(auth()->user()->isAdmin())
                                        <a href="#" class="btn btn-sm btn-danger" data-toggle="modal"
                                           data-target="#questionModal-{{ $item->id }}"><i class="fas fa-trash"></i></a>

                                        <div class="modal fade" id="questionModal-{{ $item->id }}" tabindex="-1"
                                             role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-xl" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Вы уверены что хотите удалить?</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-right">
                                                        <a class="btn btn-sm btn-danger deleteButton"
                                                           data-dismiss="modal"
                                                           data-id="{{$item->id}}"
                                                           href="#"><i class="fas fa-trash"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="col-12">
            {{$projects->links()}}
        </div>
    </div>

    <script>
        $(document).ready(() => {
            $.noConflict();
            $('#dtMaterialDesignExample').DataTable( {
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
            } );
            $('.deleteButton').on('click', (e) => {
                const projectId = $(e.currentTarget).data('id');
                $.ajax({
                    type: 'delete',
                    url: '{{url("project/")}}' + '/' + projectId,
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    data: {},
                    success: function (response) {
                        $('#' + projectId).remove();
                        console.log(response);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });
        });
    </script>
@endsection
