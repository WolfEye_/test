@extends('layouts.app')

@section('content')
    <div class="row col-12">
        <form id="form"
              class="col-lg-5 mx-auto"
              enctype="multipart/form-data"
              method="POST">
            @csrf
            @method('PATCH')

            @if (isset($errors) ? $errors->any() : false)
                <div class="col-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if (\Session::has('message'))
                <div class="col-12">
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ \Session::get('message')}}</li>
                        </ul>
                    </div>
                </div>
            @endif
            <div class="form-group row">
                <label for="title" class="col-sm-3 col-form-label">Title:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="title"
                           name="name"
                           class="form-control"
                           placeholder="Enter title..."
                           value="{{ $project->name }}"
                           required>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-3 col-form-label">Description:</label>
                <div class="col-sm-9">
                    <textarea type="text"
                              id="description"
                              maxlength="2000"
                              name="description"
                              class="form-control"
                              placeholder="Enter description..."
                              style="resize: none"
                              required>{{ $project->description }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="organization" class="col-sm-3 col-form-label">Organization:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="organization"
                           name="organization"
                           class="form-control"
                           placeholder="Enter organization..."
                           value="{{ $project->organization }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="start" class="col-sm-3 col-form-label">Start time:</label>
                <div class="col-sm-9">
                    <input type="date"
                           id="start"
                           name="start"
                           class="form-control"
                           placeholder="Enter start time..."
                           value="{{ $project->start }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="end" class="col-sm-3 col-form-label">End time:</label>
                <div class="col-sm-9">
                    <input type="date"
                           id="end"
                           name="end"
                           class="form-control"
                           placeholder="Enter end time..."
                           value="{{ $project->end }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="role" class="col-sm-3 col-form-label">Role:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="role"
                           name="role"
                           class="form-control"
                           placeholder="Enter role..."
                           value="{{ $project->role }}"
                           required>
                </div>
            </div>
            <div class="form-group row">
                <label for="link" class="col-sm-3 col-form-label">Link:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="link"
                           name="link"
                           class="form-control"
                           placeholder="Enter link..."
                           value="{{ $project->link }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="skills" class="col-sm-3 col-form-label">Skills:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="skills"
                           name="skills"
                           class="form-control"
                           placeholder="Enter skills like: planing, time managment, ..."
                           value="{{ $skills }}"
                    >
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Type:</label>
                <div class="col-sm-9">
                    <select id="type" name="type" class="form-control" required>
                        <option value="" disabled selected>Choose your option</option>
                        @foreach($types as $type)
                            <option
                                {{ $project->type === strtolower($type) ? 'selected="selected"' : '' }}
                                value="{{ strtolower($type) }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Attachments:</label>
                <div class="col-sm-9">
                    <table class="table">
                        <tbody>
                        @if (!count($project->files))
                            <tr>
                                <td colspan="3" class="text-center">Empty...</td>
                            </tr>
                        @else
                            <div class="dropdown show">
                                <div class="dropdown-menu-left" aria-labelledby="dropdownMenuLink"
                                     style="width: 500px;">
                                    @foreach ($project->files as $file)
                                        <tr id="{{ $file->id }}">
                                            <th>{{ $file->name }}</th>
                                            @if(auth()->user()->isAdmin())
                                                <td class="text-right">
                                                    <div class="btn-group text-left">
                                                        <a href="#" class="btn btn-sm btn-danger" data-toggle="modal"
                                                           data-target="#questionModal-{{ $file->id }}"><i
                                                                class="fas fa-trash"></i></a>
                                                        <div class="modal fade" id="questionModal-{{ $file->id }}"
                                                             tabindex="-1"
                                                             role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog modal-xl" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title">Вы уверены что хотите
                                                                            удалить?</h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-right">
                                                                        <a class="btn btn-sm btn-danger deleteButton"
                                                                           data-dismiss="modal"
                                                                           data-id="{{ $file->id }}"
                                                                           href="#"><i
                                                                                class="fas fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Attachments:</label>
                <div class="col-sm-9">
                    <input type="file"
                           id="attachments"
                           name="attachments[]"
                           class="form-control-file"
                           accept=".jpg, .png, .doc, .docx, .pdf"
                           multiple>
                </div>
            </div>
            <div class="form-group row">
                <a class="btn btn-primary btn-lg"
                   href="#"
                   id="editProject">Edit</a>
            </div>
        </form>
    </div>


    <script>
        $(document).ready(() => {
            const inputs = [
                $('#title'),
                $('#description'),
                $('#role'),
                $('#type')
            ];

            const errors = [];

            $('#editProject').on('click', (e) => {
                e.preventDefault();

                let success = true;

                inputs.forEach(function (item) {
                    item.val() === '' ? success = false : '';
                    item.val() === '' ? errors.push(item.attr('id')) : '';
                });

                if (success) {
                    let formData = new FormData(document.getElementById('form'));
                    formData.append('id',{{$project->id}});
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-Token': '{{ csrf_token() }}',
                            'Method': 'PATCH',
                        },
                        url: '{{url("project")}}' + '/' + '{{$project->id}}',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            console.log(response);
                            location.reload();
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });
                } else {
                    errors.forEach(function (item) {
                        $(`#${item}`).addClass('alert alert-danger')
                    });
                }
            });
            $('.deleteButton').on('click', (e) => {
                const fileId = $(e.currentTarget).data('id');
                $.ajax({
                    type: 'delete',
                    url: '{{url("project/fileDestroy")}}' + '/' + fileId,
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    data: {},
                    success: function (response) {
                        $('#' + fileId).remove();
                        console.log(response);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });
        });
    </script>
@endsection
