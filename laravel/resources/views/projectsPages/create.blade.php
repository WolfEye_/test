@extends('layouts.app')

@section('content')
    <div class="row col-12">
        <form id="form"
              class="col-lg-5 mx-auto"
              action="{{ route('project.store') }}"
              enctype="multipart/form-data"
              method="POST">
            @csrf
            @method('POST')


            @if (isset($errors) ? $errors->any() : false)
                <div class="col-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if (\Session::has('message'))
                <div class="col-12">
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ \Session::get('message')}}</li>
                        </ul>
                    </div>
                </div>
            @endif
            <div class="form-group row">
                <label for="title" class="col-sm-3 col-form-label">Title:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="title"
                           name="title"
                           class="form-control"
                           placeholder="Enter title..."
                           required>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-3 col-form-label">Description:</label>
                <div class="col-sm-9">
                    <textarea type="text"
                              id="description"
                              maxlength="2000"
                              name="description"
                              class="form-control"
                              placeholder="Enter description..."
                              style="resize: none"
                              required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="organization" class="col-sm-3 col-form-label">Organization:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="organization"
                           name="organization"
                           class="form-control"
                           placeholder="Enter organization...">
                </div>
            </div>
            <div class="form-group row">
                <label for="start" class="col-sm-3 col-form-label">Start time:</label>
                <div class="col-sm-9">
                    <input type="date"
                           id="start"
                           name="start"
                           class="form-control"
                           placeholder="Enter start time...">
                </div>
            </div>
            <div class="form-group row">
                <label for="end" class="col-sm-3 col-form-label">End time:</label>
                <div class="col-sm-9">
                    <input type="date"
                           id="end"
                           name="end"
                           class="form-control"
                           placeholder="Enter end time...">
                </div>
            </div>
            <div class="form-group row">
                <label for="role" class="col-sm-3 col-form-label">Role:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="role"
                           name="role"
                           class="form-control"
                           placeholder="Enter role..."
                           required>
                </div>
            </div>
            <div class="form-group row">
                <label for="link" class="col-sm-3 col-form-label">Link:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="link"
                           name="link"
                           class="form-control"
                           placeholder="Enter link...">
                </div>
            </div>
            <div class="form-group row">
                <label for="skills" class="col-sm-3 col-form-label">Skills:</label>
                <div class="col-sm-9">
                    <input type="text"
                           id="skills"
                           name="skills"
                           class="form-control"
                           placeholder="Enter skills like: planing, time managment, ...">
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-3 col-form-label">Type:</label>
                <div class="col-sm-9">
                    <select id="type" name="type" class="form-control" required>
                        <option value="" disabled selected>Choose your option</option>
                        @foreach($types as $type)
                            <option value="{{ strtolower($type) }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="attachments" class="col-sm-3 col-form-label">Attachments:</label>
                <div class="col-sm-9">
                    <input type="file"
                           id="attachments"
                           name="attachments[]"
                           class="form-control-file"
                           accept=".jpg, .png, .doc, .docx, .pdf"
                           multiple>
                </div>
            </div>
            <div class="form-group row">
                <a class="btn btn-primary btn-lg"
                   id="createProject"
                   href="#">Create</a>
            </div>
        </form>
    </div>


    <script>
        $(document).ready(() => {
            $('#createProject').on('click', (e) => {
                e.preventDefault();

                const requiredInputs = [
                    $('#title'),
                    $('#description'),
                    $('#role'),
                    $('#type')
                ];

                const errors = [];
                let success = true;

                requiredInputs.forEach(function (item) {
                    item.val() === '' ? success = false : '';
                    item.val() === '' ? errors.push(item.attr('id')) : '';
                });

                if (success) {
                    let formData = new FormData(document.getElementById('form'));
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-Token': '{{ csrf_token() }}',
                        },
                        url: '{{url("project")}}',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            window.location.replace('{{url("project")}}');                        },
                        error: function () {
                            console.log('fail');
                        }
                    });
                } else {
                    errors.forEach(function (item) {
                        $(`#${item}`).addClass('alert alert-danger')
                    });
                }
            })
        });
    </script>
@endsection
