@extends('layouts.app')

@section('content')
    <div class="row col-12">
        <form id="form"
              class="col-lg-5 mx-auto"
              action="{{ route('project.store') }}"
              enctype="multipart/form-data"
              method="POST">
            @csrf
            @method('POST')


            @if (isset($errors) ? $errors->any() : false)
                <div class="col-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if (\Session::has('message'))
                <div class="col-12">
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ \Session::get('message')}}</li>
                        </ul>
                    </div>
                </div>
            @endif
            <label for="csv" class="col-sm-5 col-form-label">Add CSV/EXCEL file:</label>
            <div class="form-group row">
                <div class="col-sm-9">
                    <input type="file"
                           id="csv"
                           name="csv"
                           class="form-control-file"
                           accept=".csv"
                           multiple>
                </div>
            </div>
            <div class="form-group row">
                <a class="btn btn-primary btn-lg"
                   id="createProject"
                   href="#">Create</a>
            </div>
        </form>
    </div>


    <script>
        $(document).ready(() => {
            $('#createProject').on('click', (e) => {
                e.preventDefault();

                const requiredInputs = [
                    $('#csv'),
                ];

                const errors = [];
                let success = true;

                requiredInputs.forEach(function (item) {
                    item.val() === '' ? success = false : '';
                    item.val() === '' ? errors.push(item.attr('id')) : '';
                });

                if (success) {
                    let formData = new FormData(document.getElementById('form'));
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-Token': '{{ csrf_token() }}',
                        },
                        url: '{{url("csvCreate")}}',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            window.location.replace('{{url("project")}}');
                            // console.log(response);
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });
                } else {
                    errors.forEach(function (item) {
                        $(`#${item}`).addClass('alert alert-danger')
                    });
                }
            })
        });
    </script>
@endsection
