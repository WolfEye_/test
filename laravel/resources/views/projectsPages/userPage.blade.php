@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row my-2">
            <div class="col-lg-4 order-lg-1 text-center">
                <img id="img" src="{{ $user->userImage ? $user->userImage : '//placehold.it/150'}}"
                     class="mx-auto img-fluid img-circle d-block" alt="avatar"
                        style="width: 250px; height: 250px;">
                <form id="imageForm"
                      class="col-lg-5 mx-auto"
                      enctype="multipart/form-data"
                      method="POST">
                    @csrf
                    <label for="file" class="custom-file">
                        <input
                            type="file"
                            id="file"
                            name="file"
                            accept=".jpg, .png, .bmp, .jpeg"
                            onchange="event.preventDefault();
                                document.getElementById('submitImage').click();"
                            class="custom-file-input">
                        <input type="submit" id="submitImage" hidden>
                        <span class="custom-file-control text-info"
                              style="cursor: pointer"><i class="fas fa-edit"></i>Choose Avatar</span>
                    </label>
                </form>
                <span class="custom-file-control text-info"
                      style="cursor: pointer"
                      onclick="event.preventDefault();
                                document.getElementById('submitImage').click();"><i class="fas fa-trash"></i>Delete Avatar</span>
            </div>
            <div class="col-lg-8 order-lg-2">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit</a>
                    </li>
                </ul>
                <div class="tab-content py-4">
                    <div class="tab-pane active" id="profile">
                        <h5 class="mb-3">User Profile</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <h6 class="text-info">Name:</h6>
                                <p>
                                    {{$user->name}}
                                </p>
                                <h6 class="text-info">Email:</h6>
                                <p>
                                    {{$user->email}}
                                </p>
                                <h6 class="text-info">Role:</h6>
                                <p>
                                    {{$user->role->name}}
                                </p>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <div class="tab-pane" id="edit">
                        <form id="form"
                              class="col-lg-5 mx-auto"
                              enctype="multipart/form-data"
                              method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Name</label>
                                <div class="col-lg-9">
                                    <input class="form-control"
                                           type="text"
                                           name="name"
                                           value="{{$user->name}}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Email</label>
                                <div class="col-lg-9">
                                    <input class="form-control"
                                           type="email"
                                           name="email"
                                           value="{{$user->email}}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Username</label>
                                <div class="col-lg-9">
                                    <input class="form-control"
                                           type="text"
                                           name="userName"
                                           value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Password</label>
                                <div class="col-lg-9">
                                    <input class="form-control"
                                           id="password"
                                           type="password"
                                           name="password"
                                           value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                <div class="col-lg-9">
                                    <input class="form-control"
                                           id="passwordConfirm"
                                           type="password"
                                           name="passwordConfirm"
                                           value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                <div class="col-lg-9">
                                    <input type="reset" class="btn btn-secondary" value="Cancel">
                                    <input type="submit" class="btn btn-primary" value="Save Changes">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(() => {
            $('#imageForm').submit((e) => {
                e.preventDefault();
                let formData = new FormData(document.getElementById('imageForm'));
                formData.append('id',{{$user->id}});
                $.ajax({
                    type: 'POST',
                    url: '{{url("userImage")}}',
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        console.log(response);
                        $('#img').attr('src', response);
                        $('#file').val('');
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });


            $('#form').submit((e) => {
                e.preventDefault();

                const inputs = [
                    $('#name'),
                    $('#email'),
                ];

                const password = $('#password').val();
                const passwordConfirm = $('#passwordConfirm').val();

                const errors = [];
                let success = true;

                inputs.forEach(function (item) {
                    item.val() === '' ? success = false : '';
                    item.val() === '' ? errors.push(item.attr('id')) : '';
                });

                success = password === passwordConfirm;
                success = password === passwordConfirm ? true : errors.push('password');

                if (success) {
                    let formData = new FormData(document.getElementById('form'));
                    formData.append('id',{{$user->id}});
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-Token': '{{ csrf_token() }}',
                            'Method': 'PATCH',
                        },
                        url: '{{url("user")}}' + '/' + '{{$user->id}}',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            console.log(response);
                            location.reload();
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });
                } else {
                    errors.forEach(function (item) {
                        $(`#${item}`).addClass('alert alert-danger')
                    });
                }
            });
        });
    </script>
@endsection
