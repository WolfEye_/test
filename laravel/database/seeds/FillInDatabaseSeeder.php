<?php

use App\Models\Project;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class FillInDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 20; $i++) {
            $project = Project::create([
                'name' => $faker->title,
                'user_id' => $faker->numberBetween(1,10),
                'description' => $faker->text,
                'organization' => $faker->word,
                'start' => $faker->date(),
                'end' => $faker->date(),
                'role' => $faker->word,
                'link' => $faker->url,
                'type' => 'work',
            ]);
        }

        $roles = [
            'admin',
            'user'
        ];
        foreach ($roles as $role){
            Role::create([
                'name' => $role
            ]);
        }

        for ($i = 0; $i < 10; $i++) {
            $user = User::create([
                'role_id' => 2,
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => '$2y$10$L5WpMwRpgPKxQ3e9A3mgleZJ7UNqiLLE4Aq9ZIHWQgKPoXI9CCpJ2'
            ]);
        }


    }
}
