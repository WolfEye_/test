<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const Admin = 'admin';
    const User = 'user';

    public $timestamps = false;

    protected $table = 'roles';

    protected $fillable = [
        'name'
    ];

}
