<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{

    protected $table = 'skills';

    use SoftDeletes;

    protected $fillable = [
        'name','project_id'
    ];

    /**
     * The skills that belong to the project.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
