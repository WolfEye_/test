<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    protected $table = 'files';

    use SoftDeletes;

    protected $fillable = [
        'name','project_id'
    ];

    /**
     * The files that belong to the project.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
