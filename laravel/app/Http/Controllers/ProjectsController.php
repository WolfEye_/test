<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectCreateRequest;
use App\Http\Requests\ProjectUpdateRequest;
use App\Models\File;
use App\Models\Project;
use App\Models\Skill;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;
use Dompdf\Dompdf;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $projects = auth()->user()->isAdmin() ?
            Project::paginate(10) :
            auth()->user()->projects()->paginate(10);
        return view('projectsPages.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $types = ['Work', 'Book', 'Course', 'Blog', 'Other'];
        return view('projectsPages.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function csvCreate()
    {
        return view('projectsPages.csvCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function csvCreateStore(Request $request)
    {
        if (!count($request->files)) {
            return response()->json(['message' => 'error']);

        }
        $file = $request->files->all()['csv'];
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $userId = auth()->user()->id;
        $public = public_path();

        $file->move("{$public}/uploadsCSV/", "{$userId}_{$filename}.{$extension}");

        $stream = fopen("{$public}/uploadsCSV/{$userId}_{$filename}.{$extension}", 'r');
        $csv = Reader::createFromStream($stream);
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        //build a statement
        $stmt = (new Statement())
            ->offset(0)
            ->limit(25);

        //query your records from the document
        $records = $stmt->process($csv);
        $recordsArray = [];
        foreach ($records as $record) {
            if (isset($record['title'], $record['description'], $record['role'], $record['type'])) {
                $project = Project::firstOrCreate([
                    'user_id' => auth()->user()->id,
                    'name' => $record['title'],
                    'description' => $record['description'],
                    'organization' => $record['organization'],
                    'start' => $record['start'],
                    'end' => $record['end'],
                    'role' => $record['role'],
                    'link' => $record['link'],
                    'type' => $record['type'],
                ]);

                $skills = explode(',', $record['skills']);
                if (count($skills)) {
                    foreach ($skills as $skill) {
                        if (!$skill) {
                            continue;
                        }
                        $skillsInstances[] = Skill::firstOrCreate([
                            'name' => $skill,
                            'project_id' => $project->id,
                        ]);
                    }
                }
            }
        }
        return response()->json(['message' => 'success']);
    }

    /**
     * Export project data to pdf.
     *
     * @param $id
     * @return Response
     */
    public function projectToPDF($id)
    {
        $project = Project::findOrFail($id);
        // instantiate and use the dompdf class
        $skillsData = $project->skills;
        $skills = '';
        foreach ($skillsData as $skill) {
            $skills = $skills . $skill->name . ',';
        }
        $dompdf = new Dompdf();
        $dompdf->loadHtml("
                    <h3>Project Name:</h3>
                    <p>$project->name</p>
                    <h4>Description:</h4>
                    <p>$project->description</p>
                    <h4>Organization: $project->organization</h4>
                    <h4>Project role: $project->role</h4>
                    <h4>Project start at: $project->start</h4>
                    <h4>Project end at: $project->end</h4>
                    <h4>Project link: $project->link</h4>
                    <h4>Project type: $project->type</h4>
                    <h4>Project skills: $skills</h4>           
                    <h4>Project created at: $project->created_at</h4>
                    <h4>Project updated at: $project->updated_at</h4>
        ");

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

        return response()->json(['message' => 'success']);
    }

    /**
     * Export project data to pdf.
     *
     * @param $id
     * @return Response
     */
    public function allProjectsToCSV()
    {
        $projects = auth()->user()->isAdmin() ? Project::all() : auth()->user()->projects;
        $header = ['id','user_id' , 'name', 'description', 'organization', 'start', 'end', 'role', 'link', 'type', 'deleted_at', 'created_at', 'updated_at', 'skills'];

        $csvData = [];
        $csvData[] = implode(";;", $header);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="sample.csv"');
        foreach ($projects as $project) {
            $projectData = json_decode($project,true);

            $skillsData = $project->skills;
            $skills = '';
            foreach ($skillsData as $skill) {
                $skills .= $skill->name . ',';
            }
            $projectStr = implode(";;", $projectData);

            $projectStr = $projectStr . ';;' . $skills;

            $csvData[] = $projectStr;
        }

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="AllProjects.csv"');

        $fp = fopen('php://output', 'wb');
        foreach ( $csvData as $line ) {
            $val = explode(";;", $line);
            fputcsv($fp, $val, ';');
        }
        fclose($fp);

        return response()->json('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectCreateRequest $request
     * @return Response
     */
    public function store(ProjectCreateRequest $request)
    {
        $project = Project::firstOrCreate([
            'user_id' => auth()->user()->id,
            'name' => $request->title,
            'description' => $request->description,
            'organization' => $request->organization,
            'start' => $request->start,
            'end' => $request->end,
            'role' => $request->role,
            'link' => $request->link,
            'type' => $request->type,
        ]);

        if (count($request->files)) {
            foreach ($request->files->all()['attachments'] as $file) {
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

                $userId = auth()->user()->id;
                $public = public_path();
                $file->move("{$public}/uploads/{$project->id}", "{$project->id}_{$userId}_{$filename}.{$extension}");

                $files[] = File::firstOrCreate([
                    'name' => "{$project->id}_{$userId}_{$filename}.{$extension}",
                    'project_id' => $project->id,
                ]);
            }
        }

        $skills = explode(',', $request->skills);
        if (count($skills)) {
            foreach ($skills as $skill) {
                if (!$skill) {
                    continue;
                }
                $skillsInstances[] = Skill::firstOrCreate([
                    'name' => $skill,
                    'project_id' => $project->id,
                ]);
            }
        }

        return response()->json(['message' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return Response
     */
    public function show(Project $project)
    {
        return view('projectsPages.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Response
     */
    public function edit(Project $project)
    {
        $types = ['Work', 'Book', 'Course', 'Blog', 'Other'];
        $skillsData = $project->skills;
        $skills = '';
        foreach ($skillsData as $skill) {
            $skills .= $skill->name . ',';
        }

        return view('projectsPages.edit', compact('project', 'types', 'skills'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectUpdateRequest $request
     * @param Project $project
     * @return Response
     */
    public function update(ProjectUpdateRequest $request, Project $project)
    {
        $project->update($request->except('skills'));

        if (count($request->files)) {
            foreach ($request->files->all()['attachments'] as $file) {
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

                $userId = auth()->user()->id;
                $public = public_path();
                $file->move("{$public}/uploads/{$project->id}/", "{$project->id}_{$userId}_{$filename}.{$extension}");

                $files[] = File::firstOrCreate([
                    'name' => "{$project->id}_{$userId}_{$filename}.{$extension}",
                    'project_id' => $project->id,
                ]);
            }
        }

        $skills = explode(',', str_replace(' ', '', $request->input('skills')));
        if (count($skills)) {
            foreach ($skills as $skill) {
                if (!$skill) {
                    continue;
                }
                $skillsInstances[] = Skill::firstOrCreate([
                    'name' => $skill,
                    'project_id' => $project->id,
                ]);
            }
        }

        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return Response
     * @throws \Exception
     */
    public function destroy(Project $project)
    {
        $project->delete();
        $project->files()->delete();
        $project->skills()->delete();
        return response()->json(['message' => 'Deleted success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function fileDestroy($id)
    {
        $file = File::findOrFail($id);
        $public = public_path();
        $fileDeleted = unlink("{$public}/uploads/{$file->project_id}/{$file->name}");
        $file->delete();
        return response()->json(['message' => 'Deleted success']);
    }
}
