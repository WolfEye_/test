<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersUpdateRequest;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = User::find(auth()->user()->id);
        return view('projectsPages.userPage', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsersUpdateRequest $request
     * @param User $user
     * @return Response
     */
    public function update(UsersUpdateRequest $request, User $user)
    {
        $user->update($request->input());
        if (!empty($request->input('password'))) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        return response()->json(['message' => 'success']);
    }

    /**
     * Create or update user image.
     *
     * @param Request $request
     * @return Response
     */
    public function userImage(Request $request)
    {
        $user = User::find($request->id);
        $public = public_path();
        $str = explode('/', $user->userImage);
        $str = $str[count($str) - 1];
        if ($user->userImage) {
            $userImage = unlink("{$public}/userImages/{$request->id}/{$str}");
        }
        if (count($request->files->all())) {
            $file = $request->files->all()['file'];
            $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

            $file->move("{$public}/userImages/{$request->id}/", "{$request->id}_{$filename}.{$extension}");

            $user->update(['userImage' => "/userImages/{$request->id}/{$request->id}_{$filename}.{$extension}"]);

            return response()->json("/userImages/{$request->id}/{$request->id}_{$filename}.{$extension}");
        }
        $user->update(['userImage' => '']);
        return response()->json('//placehold.it/150');
    }
}
