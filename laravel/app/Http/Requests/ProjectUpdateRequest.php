<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:projects,id',
            'name' => 'required|min:2|max:100',
            'description' => 'required|max:2000',
            'organization' => 'max:100|',
            'start' => 'max:20|',
            'end' => 'max:20|',
            'role' => 'required|',
            'link' => 'max:200|',
            'type' => 'required|'
        ];
    }
}
